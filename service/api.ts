const baseUrl: any = process.env.NEXT_PUBLIC_API_URL;

export const getMetrics = async () => {
  try {
    const res = await fetch(`${baseUrl}/28b9f0b9-47ec-4610-bcd4-143d24fca317`, {
      cache: "no-cache",
    });
    return res?.json();
  } catch (err) {
    console.error(err);
  }
};

export const getAdditionalInfo = async () => {
  try {
    const res = await fetch(`${baseUrl}/86320c66-c0ff-4ba7-bba5-4411dd96dff1`, {
      cache: "no-cache",
    });
    return res?.json();
  } catch (err) {
    console.error(err);
  }
};

export const getGraphDataset = async () => {
  try {
    const res = await fetch(`${baseUrl}/7a90d701-6937-41ca-afba-433f8956c661`, {
      cache: "no-cache",
    });
    return res?.json();
  } catch (err) {
    console.error(err);
  }
};

export const getUsers = async () => {
  try {
    const res = await fetch(`${baseUrl}/ff8eba99-4e9b-426c-86fa-73c801cfc010`, {
      cache: "no-cache",
    });
    return res?.json();
  } catch (err) {
    console.error(err);
  }
};

