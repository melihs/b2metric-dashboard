import type { Config } from "tailwindcss";
import { nextui } from "@nextui-org/react";

const config: Config = {
  important: true,
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "gray-1": "#DDE2FF",
        "gray-2": "#A4A6B3",
        "gray-3": "#363740",
        "gray-4": "#9FA2B4",
        "hover-gray": "rgba(159, 162, 180, 0.1)",
        "black-1": "#252733",
        "blue-1":"#3751FF",
        "green-1":"#29CC97"
      },
      fontSize: {
        "40": "2.5rem",
      },
    },
  },
  plugins: [nextui()],
};
export default config;
