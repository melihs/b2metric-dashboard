export interface MetricProps {
  data: IMetric[];
  className?: string;
}

export interface IMetric {
  title: string;
  data: string;
}
