import Card from "@/components/card/Card";
import { MetricProps, IMetric } from "@/components/metric/types";

const Metric = ({ data, className = "" }: MetricProps) => {
  return (
    <div className={`grid ${className}`}>
      {data?.map((metric: IMetric, key) => (
        <Card
          key={key}
          title={metric?.title}
          content={metric?.data}
          titleClass="text-gray-4 group-hover:text-blue-500 text-lg"
          contentClass="group-hover:text-blue-500 lg:text-40 text-black-1"
          className="cursor-pointer rounded-md border border-gray-200 bg-white py-6 px-7 hover:border-blue-500"
        />
      ))}
    </div>
  );
};

export default Metric;
