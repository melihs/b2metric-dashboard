"use client";
import {
  Title,
  Tooltip,
  Legend,
  Filler,
  LineElement,
  LinearScale,
  PointElement,
  CategoryScale,
  Chart as ChartJS,
} from "chart.js";
import map from "lodash/map";
import get from "lodash/get";
import { Line } from "react-chartjs-2";

import { GraphicProps } from "@/components/graphic/types";

const Graphic = ({ data }: GraphicProps) => {
  ChartJS.register(
    Title,
    Filler,
    Tooltip,
    Legend,
    LinearScale,
    LineElement,
    PointElement,
    CategoryScale,
  );

  const options = {
    fill: false,
    tension: 0.3,
    animation: false,
    responsive: true,
    pointRadius: 0,
    pointStyle: "line",
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: false,
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        grid: {
          display: true,
          position: "right",
        },
      },
    },
  };

  map(get(data, "datasets"), (dataset: any, key: number) => {
    if (key === 1) {
      dataset["fill"] = true;
      dataset["borderColor"] = "#3751ff";
      dataset["backgroundColor"] = (context: any) => {
        const ctx = context.chart.ctx;
        const gradient = ctx.createLinearGradient(0, 0, 1000, 0);
        gradient.addColorStop(0, "rgba(55, 81, 255, 0.6)");
        gradient.addColorStop(1, "rgb(255,255,255, 0.1)");
        return gradient;
      };
    }
  });

  return (
    <div className="flex flex-col p-5 lg:p-8 gap-y-7 lg:gap-y-14">
      <div className="flex justify-between items-end">
        <div className="flex flex-col text-black-1 gap-y-2">
          Today’s trends
          <p className="text-xs text-gray-4">as of 25 May 2019, 09:41 PM</p>
        </div>
        <div className="flex items-center gap-x-8 text-xs text-gray-4">
          <div className="inline-flex items-center gap-x-1">
            <span className="bg-blue-500 w-5 h-0.5"></span>
            Today
          </div>
          <div className="inline-flex items-center gap-x-1">
            <span className="bg-gray-4 w-5 h-0.5"></span>
            Yesterday
          </div>
        </div>
      </div>
      <Line data={data as any} options={options as any} />
    </div>
  );
};

export default Graphic;
