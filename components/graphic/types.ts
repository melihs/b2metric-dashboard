export interface GraphicProps {
  data: IGraphic[];
}

export interface IGraphic {
  labels: number[];
  datasets: Dataset[];
}

export interface Dataset {
  label: string;
  data: number[];
}
