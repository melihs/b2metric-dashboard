import Card from "@/components/card/Card";
import { IMetric } from "@/components/metric/types";
import { AdditionalInfoProps } from "@/components/additionalInfo/types";

const AdditionalInfo = ({ data, className = "" }: AdditionalInfoProps) => {
  return (
    <div className={`grid ${className}`}>
      {data?.map((metric: IMetric, key) => (
        <Card
          key={key}
          title={metric?.title}
          content={metric?.data}
          titleClass="text-gray-400 text-md gray-4"
          contentClass="lg:text-2xl text-black-1"
          className="lg:h-28 h-10 border-b px-3 lg:px-0 border-gray-200 bg-white"
        />
      ))}
    </div>
  );
};

export default AdditionalInfo;
