import { IMetric } from "../metric/types";

export interface AdditionalInfoProps {
  data: IMetric[];
  className?: string;
}
