"use client";
import classNames from "classnames";
import { MdClose } from "react-icons/md";

import { useApp } from "@/app/contexts/AppContext";

import Sidebar from "@/components/sidebar/Sidebar";

const NavbarMenu = () => {
  const { isOpenNavbar, hideNavbar } = useApp();

  const navClass = classNames("absolute z-50 w-screen h-screen bg-white", {
    hidden: !isOpenNavbar,
  });

  return (
    <nav className={navClass}>
      <div className="flex justify-end p-5 pt-6">
        <button onClick={hideNavbar}>
          <MdClose size={30} />
        </button>
      </div>
      <Sidebar className="bg-white" />
    </nav>
  );
};

export default NavbarMenu;
