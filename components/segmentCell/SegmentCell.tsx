import { SegmentCellProps } from "@/components/segmentCell/types";

const SegmentCell = ({ segment }: SegmentCellProps) => {
  return (
    <div className="absolute text-center text-xs w-20 py-1 px-3 bg-green-1 rounded-2xl text-white">
      {segment}
    </div>
  );
};

export default SegmentCell;
