import get from "lodash/get";
import { Input } from "@nextui-org/react";

const EmailInput = ({ handleChange, values, errors, touched }: any) => {
  return (
    <div>
      <Input
        size="lg"
        type="email"
        label="Email"
        name="email"
        labelPlacement="outside"
        placeholder="Enter your email"
        onChange={handleChange}
        value={get(values, "email")}
        errorMessage={get(errors, "email")}
        isInvalid={
          (get(errors, "email") && get(touched, "email")) as boolean
        }
      />
    </div>
  );
};

export default EmailInput;
