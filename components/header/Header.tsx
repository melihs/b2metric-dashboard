"use client";
import { MdDensityMedium } from "react-icons/md";

import { useApp } from "@/app/contexts/AppContext";

import Brand from "../brand/Brand";
import UserMenu from "../userMenu/UserMenu";
import NavbarMenu from "../navbarMenu/NavbarMenu";

const Header = () => {
  const { pageTitle, showNavbar } = useApp();

  return (
    <>
      <NavbarMenu />
      <div className="flex h-24 items-center justify-between bg-gray-100 p-7 lg:sticky lg:top-0 lg:items-start">
        <Brand className="lg:hidden" />
        <h1 className="hidden text-2xl text-black-1 font-bold lg:block capitalize">{pageTitle}</h1>
        <div className="inline-flex items-center justify-center gap-x-2">
          <UserMenu />
          <button className="lg:hidden" onClick={showNavbar}>
            <MdDensityMedium size={25} />
          </button>
        </div>
      </div>
    </>
  );
};

export default Header;
