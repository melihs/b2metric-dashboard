import Link from "next/link";
import { BrandProps } from "./types";

const Brand = ({ className = "" }: BrandProps) => {
  return (
    <Link href="/"
          className={`${className} mb-0 inline-flex items-center gap-x-3 px-0 text-lg font-medium text-gray-2 lg:px-4`}>
      <span className="h-5 w-5 rounded-full bg-blue-700"></span>
      B2Metric
    </Link>
  );
};

export default Brand;
