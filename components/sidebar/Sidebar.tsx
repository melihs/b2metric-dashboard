import Image from "next/image";

import { SidebarProps } from "./types";

import Brand from "@/components/brand/Brand";
import MenuItem from "@/components/menuItem/MenuItem";

import UserIcon from "../../public/images/user.svg";
import OverviewIcon from "../../public/images/overview.svg";

const Sidebar = ({ className = "" }: SidebarProps) => {
  return (
    <aside className={`${className}`}>
      <Brand className="hidden lg:inline-flex w-full mb-16" />
      <div className="flex flex-col">
        <MenuItem src="/" label="Overview" icon={<Image
          src={OverviewIcon}
          alt="overview"
          width={16}
          height={16}
        />} />
        <MenuItem src="/users" label="Users" icon={<Image
          src={UserIcon}
          alt="user icon"
          width={16}
          height={16}
        />} />
      </div>
    </aside>
  );
};

export default Sidebar;
