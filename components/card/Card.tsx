"use client";
import { CardProps } from "./types";

const Card = ({ className = "", titleClass = "", contentClass = "", title, content }: CardProps) => {
  return (
    <div
      className={`${className} group flex items-center justify-between lg:flex-col lg:justify-center lg:gap-y-5`}>
      <div className={`${titleClass} font-medium capitalize`}>{title}</div>
      <div className={`${contentClass} font-medium`}>{content}</div>
    </div>
  );
};

export default Card;
