export interface CardProps {
  title: string;
  content: string;
  className?: string;
  titleClass?: string;
  contentClass?: string;
}
