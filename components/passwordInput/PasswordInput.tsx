"use client";
import { useState } from "react";
import get from "lodash/get";
import { Input } from "@nextui-org/react";
import { EyeFilledIcon, EyeSlashFilledIcon } from "@nextui-org/shared-icons";

const PasswordInput = ({ handleChange, values, errors, touched }: any) => {
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const toggleVisibility = () => setIsVisible(!isVisible);

  return (
    <div className="h-20">
      <Input
        size="lg"
        label="Password"
        name="password"
        labelPlacement="outside"
        placeholder="Enter your password"
        className="outline-0"
        endContent={
          <button className="focus:outline-none outline-0" type="button" onClick={toggleVisibility}>
            {isVisible ? (
              <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
            ) : (
              <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
            )}
          </button>
        }
        type={isVisible ? "text" : "password"}
        onChange={handleChange}
        value={get(values, "password")}
        errorMessage={get(errors, "password")}
        isInvalid={
          (get(errors, "password") && get(touched, "password")) as boolean
        }
      />
    </div>
  );
};

export default PasswordInput;
