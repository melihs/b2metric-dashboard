import Image from "next/image";
import { UserCellProps } from "./types";

const UserCell = ({ userName, userImage }: UserCellProps) => {

  return (
    <span className="inline-flex items-center justify-center gap-x-5">
      <Image
        priority
        src={userImage}
        width={44} height={44} className="rounded-full"
        style={{ width: "44px", height: "44px" }}
        alt={userName} />
      {userName}
    </span>
  );
};

export default UserCell;
