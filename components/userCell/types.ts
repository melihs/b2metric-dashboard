export interface UserCellProps {
  userName: string;
  userImage: string;
}
