"use client";
import Link from "next/link";

import { MenuItemProps } from "./types";
import { useApp } from "@/app/contexts/AppContext";

const MenuItem = ({ src, label, icon }: MenuItemProps) => {
  const { hideNavbar } = useApp();

  return (
    <Link
      href={src}
      onClick={hideNavbar}
      className="w-full p-3.5 text-gray-2 hover:bg-hover-gray hover:text-gray-2"
    >
      <div className="inline-flex items-center gap-x-4">
        <span>{icon}</span>
        {label}
      </div>
    </Link>
  );
};

export default MenuItem;
