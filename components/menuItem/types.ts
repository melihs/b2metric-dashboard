import { ReactNode } from "react";

export interface MenuItemProps {
  src: string;
  label: string;
  icon: ReactNode;
}
