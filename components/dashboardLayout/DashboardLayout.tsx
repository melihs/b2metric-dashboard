"use client";
import { ReactNode } from "react";
import { redirect } from "next/navigation";
import Header from "@/components/header/Header";
import Sidebar from "@/components/sidebar/Sidebar";
import { useAuthState } from "react-firebase-hooks/auth";

import { auth } from "@/app/lib/firebaseConfig";
import Loading from "@/components/loading/Loading";

const DashboardLayout = ({ children }: Readonly<{ children: ReactNode; }>) => {
  const [user, loading] = useAuthState(auth);
  if (loading) return <Loading />;


  if (typeof window !== "undefined") {
    if (!user && !sessionStorage.getItem("user")) return redirect("/sign-in");
  }

  return (
    <main className="flex min-h-screen flex-col lg:h-screen lg:flex-row">
      <Sidebar className="hidden w-64 bg-gray-3 pt-5 text-gray-2 lg:block lg:w-56" />
      <div className="flex flex-1 flex-col overflow-hidden bg-gray-100">
        <Header />
        {children}
      </div>
    </main>
  );
};

export default DashboardLayout;
