"use client";
import { useEffect, useMemo, useState } from "react";
import {
  Table,
  TableRow,
  TableCell,
  TableBody,
  Pagination,
  TableHeader,
  getKeyValue,
  TableColumn,
} from "@nextui-org/react";
import get from "lodash/get";
import isEmpty from "lodash/isEmpty";

import { getUsers } from "@/service/api";
import { IUser, RenderCellProps } from "@/app/users/types";

import UserCell from "@/components/userCell/UserCell";
import SegmentCell from "@/components/segmentCell/SegmentCell";
import DashboardLayout from "@/components/dashboardLayout/DashboardLayout";

const User = () => {
  const [page, setPage] = useState<number>(1);
  const [users, setUsers] = useState<IUser[]>([]);

  const fetchUsers = async () => {
    const userData = await getUsers();
    !isEmpty(userData) && setUsers(userData);
  };

  useEffect(() => {
    isEmpty(users) && fetchUsers();
  }, [users]);


  const rowsPerPage = 8;
  const pages = Math.ceil(users?.length / rowsPerPage);

  const items = useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;

    return users.slice(start, end);
  }, [page, users]);

  const renderCell = ({ item, columnKey }: RenderCellProps) => {
    switch (columnKey) {
      case "userName" :
        return <UserCell userName={getKeyValue(item, columnKey)} userImage={get(item, "userImage")} />;
      case "segment":
        return <SegmentCell segment={getKeyValue(item, columnKey)} />;
      case "userImage":
        return "";
      default:
        return getKeyValue(item, columnKey);
    }
  };

  return (
    <DashboardLayout>
      <div className="flex-1 overflow-y-auto overflow-x-hidden bg-gray-100 p-7">
        <Table
          aria-label="users list table"
          topContent={
            <h1 className="text-lg mb-12">All Users</h1>
          }
          bottomContent={
            <div
              className="fixed lg:relative bg-white lg:bg-none p-2 bottom-0 lg:bottom-1 z-50 flex w-screen lg:w-full mx-auto lg:mx-0 left-0 justify-center">
              <Pagination
                isCompact
                showControls
                showShadow
                color="primary"
                page={page}
                total={pages}
                onChange={(page) => setPage(page)}
                classNames={{ wrapper: "bg-white flex w-full justify-center" }}
              />
            </div>
          }
          classNames={{
            wrapper: "min-h-[222px] lg:p-8",
          }}
        >
          <TableHeader>
            <TableColumn className="bg-white capitalize border-b border-gray-1 text-sm text-gray-4" key="userName">user
              name</TableColumn>
            <TableColumn className="bg-white capitalize border-b border-gray-1 text-sm text-gray-4"
                         key="customerName">customer
              name</TableColumn>
            <TableColumn className="bg-white capitalize border-b border-gray-1 text-sm text-gray-4"
                         key="registerDate">register
              date</TableColumn>
            <TableColumn className="bg-white capitalize border-b border-gray-1 text-sm text-gray-4"
                         key="segment">segment</TableColumn>
          </TableHeader>
          <TableBody items={items} emptyContent={"No rows to display."}>
            {(item: IUser) => (
              <TableRow key={get(item, "userName")} className="hover:bg-gray-100">
                {(columnKey) => <TableCell className="py-4 border-b border-b-gray-1">
                  {renderCell({ item, columnKey })}
                </TableCell>}
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    </DashboardLayout>
  );
};

export default User;

