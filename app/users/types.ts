export interface IUser {
  userName: string;
  userImage: string;
  customerName: string;
  registerDate: string;
  segment: string;
}

export interface RenderCellProps {
  item: IUser;
  columnKey: string | number;
}
