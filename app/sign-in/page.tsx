"use client";
import * as Yup from "yup";
import Link from "next/link";
import get from "lodash/get";
import { useFormik } from "formik";
import { redirect } from "next/navigation";
import toast, { Toaster } from "react-hot-toast";
import { Button } from "@nextui-org/react";
import { useAuthState, useSignInWithEmailAndPassword } from "react-firebase-hooks/auth";

import { auth } from "../lib/firebaseConfig";

import Loading from "@/components/loading/Loading";
import PasswordInput from "@/components/passwordInput/PasswordInput";
import EmailInput from "@/components/emailInput/EmailInput";

const Signin = () => {
  const [user, loading] = useAuthState(auth);
  const [signInWithEmailAndPassword] = useSignInWithEmailAndPassword(auth);

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("invalid or incomplete email")
      .max(50, "email is too long")
      .required("required"),
    password: Yup.string()
      .min(6, "password is too short must be at least 6 characters")
      .required("required"),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema,
    onSubmit: async ({ email, password }: { email: string, password: string }) => {
      const res = await signInWithEmailAndPassword(email, password);

      if (get(res, "user")) {
        // @ts-ignore
        sessionStorage.setItem("user", true);
        redirect("/");
      } else {
        toast.error("Check your user information!");
      }
    },
  });

  const { errors, values, touched, handleSubmit, handleChange, isSubmitting } =
    formik;

  if (typeof window !== "undefined") {
    if (user && sessionStorage.getItem("user")) return redirect("/");
  }

  if (loading) return <Loading />;

  return (
    <div className="flex items-center justify-center min-h-screen bg-black">
      <div className="flex flex-col bg-white p-8 rounded-lg shadow-md w-full max-w-md">
        <div className="flex flex-col text-center gap-y-3  mb-8">
          <div className="mx-auto w-12 h-12 rounded-full bg-blue-700"></div>
          <h1 className="text-xl font-semibold text-gray-2">B2Metric</h1>
        </div>
        <div className="flex flex-col gap-y-3 text-black-1 mb-12 items-center justify-center">
          <p className="text-2xl font-bold">Login to B2Metric</p>
          <div className="text-gray-4 text-sm">Enter your email and password below</div>
        </div>
        <form noValidate onSubmit={handleSubmit} className="flex flex-col gap-y-6">
          <Toaster position="top-right" />
          <EmailInput
            handleChange={handleChange}
            values={values}
            errors={errors}
            touched={touched}
          />
          <PasswordInput
            handleChange={handleChange}
            values={values}
            errors={errors}
            touched={touched}
          />
          <Button
            fullWidth={true}
            type="submit"
            color="success"
            className="text-lg text-white bg-blue-1 px-6 py-3.5 hover:bg-blue-600 rounded-lg"
            isLoading={isSubmitting}
          >
            Login
          </Button>
        </form>
        <p className="mt-4 text-gray-600 text-center text-sm">
          Don’t have an account? <Link href="/sign-up" className="text-blue-500">Sign up</Link>
        </p>
      </div>
    </div>
  );
};

export default Signin;
