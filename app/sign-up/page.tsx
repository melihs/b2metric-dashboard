"use client";
import * as Yup from "yup";
import Link from "next/link";
import get from "lodash/get";
import { useFormik } from "formik";
import { redirect } from "next/navigation";
import { updateProfile } from "firebase/auth";
import { Button, Input } from "@nextui-org/react";
import { useAuthState } from "react-firebase-hooks/auth";

import { auth } from "../lib/firebaseConfig";
import Loading from "@/components/loading/Loading";
import { createUserWithEmailAndPassword } from "@firebase/auth";
import toast, { Toaster } from "react-hot-toast";
import EmailInput from "@/components/emailInput/EmailInput";
import PasswordInput from "@/components/passwordInput/PasswordInput";

const Signup = () => {
  const [user, loading] = useAuthState(auth);

  const validationSchema = Yup.object().shape({
    fullName: Yup.string()
      .required("required"),
    email: Yup.string()
      .email("invalid or incomplete email")
      .max(50, "email is too long")
      .required("required"),
    password: Yup.string()
      .min(6, "password is too short must be at least 6 characters")
      .required("required"),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
      fullName: "",
      password: "",
    },
    validationSchema,
    onSubmit: async ({ email, password, fullName }: { email: string, password: string, fullName: string }) => {
      createUserWithEmailAndPassword(auth, email, password)
        .then((res) => {
          updateProfile(get(auth, "currentUser") as any, {
            displayName: fullName,
            photoURL: "https://i.pravatar.cc/150?u=a04258114e29026302d",
          });
          if (get(res, "user")) {
            // @ts-ignore
            sessionStorage.setItem("user", true);
          }
        })
        .catch((error) => {
          error && toast.error(error?.message);
        });

    },
  });

  const { errors, values, touched, handleSubmit, handleChange, isSubmitting } =
    formik;

  if (typeof window !== "undefined") {
    if (user && sessionStorage.getItem("user")) return redirect("/");
  }

  if (loading) return <Loading />;

  return (
    <div className="flex items-center justify-center min-h-screen bg-black">
      <div className="flex flex-col bg-white p-8 rounded-lg shadow-md w-full max-w-md">
        <div className="flex flex-col text-center gap-y-3  mb-8">
          <div className="mx-auto w-12 h-12 rounded-full bg-blue-700"></div>
          <h1 className="text-xl font-semibold text-gray-2">B2Metric</h1>
        </div>
        <div className="flex flex-col gap-y-3 text-black-1 mb-12 items-center justify-center">
          <p className="text-2xl font-bold">Sign up to B2Metric</p>
        </div>
        <form noValidate onSubmit={handleSubmit} className="flex flex-col gap-y-6">
          <Toaster position="top-right" />
          <div>
            <Input
              size="lg"
              type="text"
              label="User name"
              name="fullName"
              labelPlacement="outside"
              placeholder="Enter your name"
              onChange={handleChange}
              value={get(values, "fullName")}
              errorMessage={get(errors, "fullName")}
              isInvalid={
                (get(errors, "fullName") && get(touched, "fullName")) as boolean
              }
            />
          </div>
          <EmailInput
            handleChange={handleChange}
            values={values}
            errors={errors}
            touched={touched}
          />
          <PasswordInput
            handleChange={handleChange}
            values={values}
            errors={errors}
            touched={touched}
          />
          <Button
            fullWidth={true}
            type="submit"
            color="success"
            className="text-lg text-white green-1 px-6 py-3.5 hover:bg-green-600 rounded-lg"
            isLoading={isSubmitting}
          >
            Signup
          </Button>
        </form>
        <p className="mt-4 text-gray-600 text-center text-sm">
          I already have an account. <Link href="/sign-in" className="text-blue-500">Sign in</Link>
        </p>
      </div>
    </div>
  );
};

export default Signup;
