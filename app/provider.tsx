"use client";
import { ReactNode } from "react";
import { NextUIProvider } from "@nextui-org/react";

import { AppProvider } from "@/app/contexts/AppContext";

const Provider = ({ children }: Readonly<{ children: ReactNode }>) => {

  return (
    <NextUIProvider>
      <AppProvider>
        {children}
      </AppProvider>
    </NextUIProvider>
  );
};

export default Provider;
