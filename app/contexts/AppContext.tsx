"use client";
import { createContext, useState, useContext, ReactNode, useEffect } from "react";
import first from "lodash/first";
import compact from "lodash/compact";
import { usePathname } from "next/navigation";

// @ts-ignore
const AppContext = createContext();

export const AppProvider = ({ children }: Readonly<{ children: ReactNode; }>) => {
  const pathname = usePathname();
  const [pageTitle, setPageTitle] = useState<string>("");
  const [isOpenNavbar, setIsOpenNavbar] = useState<boolean>(false);

  const hideNavbar = () => setIsOpenNavbar(false);

  const showNavbar = () => setIsOpenNavbar(true);

  const getCurrentPage = (): string | null | undefined => {
    return pathname ? first(compact(pathname.split("/"))) : null;
  };


  useEffect(() => {
    switch (pathname) {
      case "/users" :
        setPageTitle("user");
        break;
      case "/" :
        setPageTitle("overview");
        break;
      default:
        setPageTitle("");
        break;
    }

    if (typeof window !== "undefined") {
      document.body.style.overflow = isOpenNavbar ? "hidden" : "";
    }
  }, [pathname, isOpenNavbar]);

  const contextValue = {
    pageTitle,
    isOpenNavbar,
    showNavbar,
    hideNavbar,
    getCurrentPage,
  };

  return (
    <AppContext.Provider value={contextValue}>
      {children}
    </AppContext.Provider>
  );
};

export const useApp: any = () => {
  const context = useContext(AppContext);
  if (!context) {
    throw new Error("useApp hook must be used within a AppProvider");
  }
  return context;
};


