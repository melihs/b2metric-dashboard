import { IMetric } from "@/components/metric/types";
import { IGraphic } from "@/components/graphic/types";
import { getAdditionalInfo, getGraphDataset, getMetrics } from "@/service/api";

import Metric from "@/components/metric/Metric";
import Graphic from "@/components/graphic/Graphic";
import AdditionalInfo from "@/components/additionalInfo/AdditionalInfo";
import DashboardLayout from "@/components/dashboardLayout/DashboardLayout";

const Home = async () => {
  const metrics: IMetric[] = await getMetrics();
  const graphData: IGraphic[] = await getGraphDataset();
  const additionalInfo: IMetric[] = await getAdditionalInfo();

  return (
    <DashboardLayout>
      <div className="flex-1 overflow-y-auto overflow-x-hidden bg-gray-100 p-7">
        <Metric
          className="grid-rows-4 gap-4 lg:grid-cols-4 lg:grid-rows-1"
          data={metrics}
        />
        <div className="mt-7 flex-row lg:flex rounded-md bg-white">
          <div className="w-full border lg:w-2/3">
            <Graphic data={graphData} />
          </div>
          <div className="w-full lg:w-1/3">
            <AdditionalInfo data={additionalInfo} />
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
};

export default Home;
