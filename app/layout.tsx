import { ReactNode } from "react";
import { Inter } from "next/font/google";

import Provider from "@/app/provider";

import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

const RootLayout = ({ children }: Readonly<{ children: ReactNode; }>) => {
  return (
    <html lang="en">
    <body className={inter.className} suppressHydrationWarning={true}>
    <Provider>
      {children}
    </Provider>
    </body>
    </html>
  );
};

export default RootLayout;
