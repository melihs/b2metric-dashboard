# Study Case

## Getting Started

### Demo
https://b2metric-dashboard.vercel.app

### Run

```
$ git clone https://gitlab.com/melihs/b2metric-dashboard.git
$ cd b2metric-dashboard

$ yarn
$ yarn build
$ yarn start
```
